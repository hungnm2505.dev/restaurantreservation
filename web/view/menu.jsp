<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Sam bà tỵ - Our Menus</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/hero-slider.css">
        <link rel="stylesheet" href="css/owl-carousel.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Spectral:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

<body>
    <div class="header">
        <div class="container">
            <a href="#" class="navbar-brand scroll-top">Sam bà tỵ menu</a>
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!--/.navbar-header-->
                                  <div id="main-nav" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="home">Home</a></li>
                            <li><a href="menu">Our Menu</a></li>
                            <li><a href="https://www.facebook.com/dacsansamhalong">Social Media</a></li>
                            <li><a href="book1">Booking</a></li>
                            <c:if test="${sessionScope.auth==null}"><li><a href="login">Login</a></li></c:if>
                            <c:if test="${sessionScope.auth!=null}"><li><a href="admin">Admin page</a></li>&nbsp;<li><a href="logout">Logout</a></li></c:if>

                        </ul>
                    </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </div>
    <!--/.header-->


    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Our Menu</h1>
                    <p>We bring you the best experience</p>
                </div>
            </div>
        </div>
    </section>



    <section class="breakfast-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="breakfast-menu-content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="left-image">
                                    <img src="img/breakfast_menu.jpg" alt="Breakfast">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <h2>Món khai vị</h2>
                                <div id="owl-breakfast" class="owl-carousel owl-theme">
                                    <c:forEach items="${requestScope.lsDishes}" var="d">
                                        <c:if test="${d.status==2}">
                                    <div class="item col-md-12">
                                        <div class="food-item">
                                            <img src="img/${d.imgName}" alt="">
                                            <div class="price">${d.price}</div>
                                            <div class="text-content">
                                                <h4>${d.name}</h4>
                                                <p>${d.description}</p>
                                            </div>
                                        </div>
                                    </div></c:if>
                                            </c:forEach>    
                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="lunch-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="lunch-menu-content">
                        <div class="row">
                            <div class="col-md-7">
                                <h2>Món chính</h2>
                                <div id="owl-lunch" class="owl-carousel owl-theme">
                                    <c:forEach items="${requestScope.lsDishes}" var="d">
                                        <c:if test="${d.status==1}">
                                    <div class="item col-md-12">
                                        <div class="food-item">
                                            <img src="img/${d.imgName}" alt="">
                                            <div class="price">${d.price}</div>
                                            <div class="text-content">
                                                <h4>${d.name}</h4>
                                                <p>${d.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                    </c:if>
                                    </c:forEach>
                                    
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="left-image">
                                    <img src="img/lunch_menu.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dinner-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="dinner-menu-content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="left-image">
                                    <img src="img/dinner_menu.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <h2>Món tráng miệng</h2>
                                <div id="owl-dinner" class="owl-carousel owl-theme">
                                    <c:forEach items="${requestScope.lsDishes}" var="d">
                                        <c:if test="${d.status==3}">
                                    <div class="item col-md-12">
                                        <div class="food-item">
                                            <img src="img/${d.imgName}" alt="">
                                            <div class="price">${d.price}</div>
                                            <div class="text-content">
                                                <h4>${d.name}</h4>
                                                <p>${d.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                    </c:if>
                                    </c:forEach>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Address: 188 group 19, block 2B, Cao Thang Ward, Ha Long City, Quang Ninh Province</p>
                </div>
                
                
                <div class="col-md-4">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/dacsansamhalong"><i class="fa fa-facebook"></i></a></li>
                        
                    </ul>
                </div>
                <div class="col-md-4">
                    <p>Designed by <em>Nguyen Minh Hung</em></p>
                </div>
            </div>
        </div>
    </footer>


    


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions 
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 750);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');         
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 0;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    </script>
</body>
</html>