/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.Table1Dao;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.table_1;

/**
 *
 * @author Hung
 */
public class BookingTable extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        ArrayList<table_1> lstb = new Table1Dao().getAllbyStt();
//        request.setAttribute("status", lstb);
//        request.getRequestDispatcher("view/Booking.jsp").forward(request, response);
  String table = request.getParameter("table");
        Table1Dao dao = new Table1Dao();
        ArrayList<table_1> list = dao.getAllbyTable(table);
        int a=list.size();
request.setAttribute("size", a);
        request.setAttribute("table", table);
        request.setAttribute("list", list);
        request.getRequestDispatcher("view/Booking.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String day = request.getParameter("day");
        String table = request.getParameter("table1");
        String hour = request.getParameter("hour");
        String name = request.getParameter("name");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        int person = Integer.parseInt(request.getParameter("persons"));

        table_1 tb = new table_1();
        tb.setDate(Date.valueOf(day));
        tb.setName(name);
        tb.setPhone(phone);
        tb.setTable(table);
        tb.setTime(hour);
        tb.setPerson(person);
        tb.setEmail(email);
        new Table1Dao().update(tb);

        
        Table1Dao dao = new Table1Dao();
        ArrayList<table_1> list = dao.getAllbyTable(table);

        request.setAttribute("table", table);
        request.setAttribute("list", list);
        request.getRequestDispatcher("view/Booking.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
