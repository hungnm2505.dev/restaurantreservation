<%-- 
    Document   : AccessDenied
    Created on : Mar 31, 2021, 1:51:22 AM
    Author     : Hung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Please Login First</h1>
        <a href="login">Return to login page</a>
    </body>
</html>
